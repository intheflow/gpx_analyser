extern crate clap;
extern crate gpx;
extern crate geo;
extern crate failure;

use std::path::Path;

use clap::{Arg, App};

mod statistics;
use statistics::gpx_track::track_statistics;

mod cleanup;

const VERSION: & str = env!("CARGO_PKG_VERSION");
const PKG_NAME: & str = env!("CARGO_PKG_NAME");
const PKG_DESCRIPTION: & str = env!("CARGO_PKG_DESCRIPTION");


fn main() {
    let matches = App::new(PKG_NAME).version(VERSION).about(PKG_DESCRIPTION)
        .arg(Arg::with_name("files").help("GPX input").required(true).min_values(1))
        .get_matches();

    let files: Vec<_> = matches.values_of("files").unwrap().collect();

    println!("Analysing {} files", files.len());

    for (i, filename) in files.iter().enumerate() {
        let file = Path::new(filename);
        if file.is_file() && file.extension().unwrap_or_default() == "gpx" {
            match track_statistics(file) {
                Ok(stat) => println!("{1:2}: {2:.2}km in {3}min ({4:.2}km/h) << {0} >>", file.file_name().unwrap().to_str().unwrap(), i+1, stat.length/1000.0, stat.duration/60, stat.speed),
                Err(err) => println!("{1:2}: {0} << {2} >>", err, i+1, file.file_name().unwrap().to_str().unwrap())
            }
        }
    }
}

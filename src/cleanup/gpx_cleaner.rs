use std::error::Error;
use std::fmt;

use gpx::Gpx;
use geo::algorithm::haversine_length::HaversineLength;



#[derive(Debug)]
pub struct GPXCleanError {
    details: String
}

impl GPXCleanError {
    fn new(msg: &str) -> GPXCleanError {
        GPXCleanError{details: msg.to_string()}
    }
}

impl fmt::Display for GPXCleanError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,"{}",self.details)
    }
}

impl Error for GPXCleanError {
    fn description(&self) -> &str {
        &self.details
    }
}

pub fn remove_empty_tracks(mut gpx: Gpx) -> Result<Gpx, GPXCleanError> {
    &gpx.tracks.retain(|track| track.multilinestring().haversine_length() > 0.0);
    if gpx.tracks.len() == 0 {
        return Err(GPXCleanError::new("Empty gpx"));
    }
    return Ok(gpx)
}

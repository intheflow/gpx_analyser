use std::fmt;

use std::path::Path;
use std::io::BufReader;
use std::fs::File;

use gpx::read;
use gpx::{Gpx, Track};
use geo::algorithm::haversine_length::HaversineLength;

use cleanup::gpx_cleaner::remove_empty_tracks;


#[derive(Debug)]
pub struct GpxStatistic {
    pub length: f64,
    pub speed: f64,
    pub duration: i64
}

#[derive(Debug)]
pub struct GPXAnalyserError {
    details: String
}

impl GPXAnalyserError {
    fn new(msg: String) -> GPXAnalyserError {
        GPXAnalyserError{details: msg}
    }
}

impl fmt::Display for GPXAnalyserError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,"{}",self.details)
    }
}


fn get_first_timestamp(track: &Track) -> Option<i64> {
    return match track.segments.first() {
        Some(segment) => match segment.points.first() {
            Some(point) => match point.time {
                Some(time) => Some(time.timestamp()),
                _ => None,
            },
            _ => None,
        },
        _ => None,
    };
}

pub fn track_statistics(file_path: &Path) -> Result<GpxStatistic, GPXAnalyserError> {
    let file = match File::open(file_path) {
        Ok(f) => f,
        Err(e) => return Err(GPXAnalyserError::new(e.to_string()))
    };
    let reader = BufReader::new(file);

    // read takes any io::Read and gives a Result<Gpx, Error>.
    let gpx: Gpx = match remove_empty_tracks(read(reader).unwrap()) {
        Ok(gpx) => gpx,
        Err(err) => return Err(GPXAnalyserError::new(err.to_string())),
    };

    let mut len_m = 0.0;

    for track in &gpx.tracks {
        len_m += track.multilinestring().haversine_length();
    }

    let start_timestamp = match get_first_timestamp(&gpx.tracks[0]) {
        Some(timestamp) => timestamp,
        None => return Err(GPXAnalyserError::new("No start timestamp".to_string())),
    };

    // FIXME don't simply unwrap
    let end = &gpx.tracks.last().unwrap().segments.last().unwrap().points.last().unwrap().time.unwrap();
    let duration = end.timestamp() - start_timestamp;

    let speed = (len_m / 1000.0) / (duration as f64 / 60.0 / 60.0);

    Ok(GpxStatistic{length: len_m, duration: duration, speed: speed})
}

# gpx_analyser: calculate statistics of gpx tracks

## Planned features

* Calculate length of tracks
* Calculate average (movement) speed
* Sum elevation gain/loss
* Trends, average in week, ...
* Maybe draw some graphs